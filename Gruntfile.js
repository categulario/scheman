module.exports = function(grunt) {

	require("load-grunt-tasks")(grunt); // npm install --save-dev load-grunt-tasks

	grunt.initConfig({
		babel: {
			options: {
				sourceMap: true
			},
			dist: {
				files: {
					"scheman/static/js/subjects.js": "src/scripts/subjects.js",
					"scheman/static/js/schedule.js": "src/scripts/schedule.js",
				}
			}
		},

		sass: {
			dist: {
				options: {},
				files: {
					'scheman/static/css/app.css': 'src/sass/app.scss',
				},
			},
		},

		concat: {
			vendorcss: {
				src: [
					'./node_modules/font-awesome/css/font-awesome.min.css',
					'./node_modules/select2/dist/css/select2.min.css',
				],
				dest: 'scheman/static/css/vendor.css',
			},
			vendorjs: {
				src: [
					'./node_modules/babel-polyfill/browser-polyfill.js',
					'./node_modules/jquery/dist/jquery.min.js',
					'./node_modules/vue/dist/vue.min.js',
					'./node_modules/bootstrap/dist/js/bootstrap.min.js',
					'./node_modules/notifyjs-browser/dist/notify.js',
					'./node_modules/moment/min/moment-with-locales.min.js',
					'./node_modules/select2/dist/js/select2.min.js',
				],
				dest: 'scheman/static/js/vendor.js',
			},
		},

		copy: {
			fontawesome: {
				files: [
					{
						expand: true,
						cwd: './node_modules/font-awesome/fonts/',
						src: ['*'],
						dest: 'scheman/static/fonts/',
						filter: 'isFile',
						flatten: true,
					}
				]
			},
		},
	});

	grunt.registerTask("default", ["babel", "sass", "concat", "copy"]);

};
