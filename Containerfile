FROM docker.io/node:20-bookworm as buildcontainer

RUN apt-get update && apt-get install -y --no-install-recommends ruby ruby-dev

RUN gem install sass

RUN mkdir /app
WORKDIR /app

COPY ./package.json ./
COPY ./package-lock.json ./

RUN npm ci

COPY src/ src/
COPY ./Gruntfile.js ./

RUN npx grunt

FROM docker.io/python:3.8-bookworm

# Set timezone
ENV TZ=UTC
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

ENV APP_NAME=scheman
ENV APP_USER=$APP_NAME
ENV APP_HOME=/var/lib/$APP_USER
ENV APP_VENV=$APP_HOME/venv
ENV APP_PUBLIC=$APP_HOME/public
ENV APP_STATIC=$APP_PUBLIC/static
ENV APP_MEDIA=$APP_HOME/media
ENV APP_CONFIG=/etc/$APP_NAME
ENV APP_DB=$APP_HOME/db

RUN useradd --system --uid 900 --home-dir $APP_HOME --create-home --shell /bin/false $APP_USER
RUN mkdir -p $APP_PUBLIC $APP_STATIC $APP_MEDIA $APP_DB

RUN chown -R $APP_USER:$APP_USER $APP_HOME

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get -q -y upgrade \
    && apt-get -q -y install --no-install-recommends \
        virtualenv \
    && apt-get -q clean \
    && rm -rf /var/lib/apt/lists/*

# Finished operations performed as root

WORKDIR $APP_HOME
USER $APP_USER

RUN virtualenv $APP_VENV

ENV PATH=$APP_VENV/bin:$PATH

RUN pip install -U pip

COPY ./requirements.txt ./
RUN pip install -r requirements.txt

# Yes, I know 'COPY .' exists, but here is the thing: many unwanted stuff gets
# copied. Yes, I know .containerignore exists, but there's a fundamental problem
# with it: new stuff that I would like to ignore needs to be added manually
# every time and it is easy to forget to do it. So, for now, I'll nanually copy
# things, which will also give some fine-grained control over the cache layers
# and how they get invalidated.
COPY ./setup.py ./
COPY scheman/ scheman/
RUN pip install -e .

COPY --from=buildcontainer /app/scheman/static/ ./scheman/static/

COPY --chmod=755 scripts/container/entrypoint.sh ./
COPY scripts/container/gunicorn_config.py $APP_CONFIG/

ENV SCHEMAN_SQLALCHEMY_DATABASE_URI=sqlite:///$APP_DB/db.sqlite3

ENTRYPOINT ["./entrypoint.sh"]
