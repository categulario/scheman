.PHONY: pytest lint test

test: lint pytest

pytest:
	pytest -xvv

lint:
	flake8 --exclude=.venv,dist,docs,build,*.egg --ignore E501 ./scheman
