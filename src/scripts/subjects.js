$(function () {
  var $hours = $('#subject-hours');
  var $mainsel = $('#subject-classroom-main');

  $hours.val(0);
  $mainsel.val('0');
  $('#subject-classroom-main').val('');
  $('.subject-classroom').val('');

  $mainsel.on('change', function (event) {
    $('.subject-classroom').val($mainsel.val()).trigger('change');
  });

  $('.subject-classroom').on('change', function (event) {
    var day = event.target.dataset.day;
    var $th = $(`#th-day-${day}`);

    $th.addClass('loading');

    $.ajax({
      url: '/api/schedule',
      data: {
        classroom_id: event.target.value,
        embed: 'subject',
        day: day,
      },
      type: 'get',
      success: function (data) {
        $(`td[data-day=${day}]`)
          .removeClass('selected')
          .removeClass('occupied')
          .removeClass('was-occupied')
          .html('');
        $th.removeClass('loading');

        data.forEach(function (item) {
          $(`td[data-day=${day}][data-hour=${item.hour}]`)
            .addClass('occupied')
            .text(item.subject.name);
        });
      },
      error: function (xhr) {
        $th.removeClass('loading');
        console.log(xhr);
      },
    });
  });

  $('.td-hover').click(function (event) {
    var $el = $(event.currentTarget);
    var room = $(`select[name=day_room_${$el.data('day')}]`).val();

    if (!room) {
      $.notify('Primero selecciona un salón para este día de la semana');
      return;
    }

    if ($el.is('.selected')) {
      $el.removeClass('selected');

      if ($el.is('.was-occupied')) {
        $el.addClass('occupied');
        $el.removeClass('was-occupied');
        $el.text($el.data('tutor'));
        $el.data('tutor', '');
      }

      $hours.val(parseInt($hours.val()) - 1);
      $el.html('');
    } else {
      $el.addClass('selected');

      if ($el.is('.occupied')) {
        $el.addClass('was-occupied');
        $el.removeClass('occupied');
        $el.data('tutor', $el.text());
      }

      $hours.val(parseInt($hours.val()) + 1);
      $el.html(`<input type="hidden" name="hour" value="h${$el.data('hour')}-d${$el.data('day')}">`);
    }
  });
});
