# Schedule Manager

A simple schedule manager

## Develop

1. Clone the repository
2. Copy `settings.py` to `settings_develop.py` and modify it
3. Create a `.env` file in the project root with the following contents:

```
export FLASK_APP=scheman
export FLASK_DEBUG=1
export SCHEMAN_SETTINGS=./settings_develop.py
```

4. Install dependencies with `pipenv install`
5. Install frontend dependencies with `npm install`
6. Compile static files with `npx grunt`
7. Migrate, seed and run the system:

```
pipenv run flask migrate
pipenv run flask seed
pipenv run flask run
```

8. Now you can visit the website at http://localhost:5000
9. You might want to create a user with `pipenv run flask create-user` and use
   it to login

## Test

    python tests.py
