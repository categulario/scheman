from setuptools import setup

setup(
    name = 'scheman',
    packages = ['scheman'],
    include_package_data = True,
    install_requires = [
        'werkzeug == 2.0.1',
        'flask == 2.0.1',
        'flask-sqlalchemy == 2.5.1',
        'sqlalchemy == 1.4.16',
        'flask-user == 1.0.2.2',
        'email-validator',
        'click',
        'wtforms == 2.3.3',
    ],
)
