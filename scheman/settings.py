import os

SECRET_KEY = os.getenv('SCHEMAN_SECRET_KEY', 'a 32 chars long and random string')
SQLALCHEMY_DATABASE_URI = os.getenv('SCHEMAN_SQLALCHEMY_DATABASE_URI', 'sqlite:///app.sqlite')
CSRF_ENABLED = os.getenv('SCHEMAN_CSRF_ENABLED', '1').lower() in ['1', 'true']
SQLALCHEMY_TRACK_MODIFICATIONS = os.getenv('SCHEMAN_SQLALCHEMY_TRACK_MODIFICATIONS', '0').lower() in ['1', 'true']

# Flask-Mail settings
MAIL_USERNAME = os.getenv('SCHEMAN_MAIL_USERNAME', '')
MAIL_PASSWORD = os.getenv('SCHEMAN_MAIL_PASSWORD', '')
MAIL_DEFAULT_SENDER = os.getenv('SCHEMAN_MAIL_DEFAULT_SENDER', '"MyApp" <noreply@example.com>')
MAIL_SERVER = os.getenv('SCHEMAN_MAIL_SERVER', 'smtp.gmail.com')
MAIL_PORT = os.getenv('SCHEMAN_MAIL_PORT', '465')
MAIL_USE_SSL = os.getenv('SCHEMAN_MAIL_USE_SSL', True)

# Flask-User settings http://pythonhosted.org/Flask-User/api.html
USER_APP_NAME = os.getenv('SCHEMAN_USER_APP_NAME', "'skɛ'mæn")  # Used by email templates
