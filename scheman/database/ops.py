from datetime import datetime

from .models import Hour, Freeroom, Classroom, Tutor, User, Subject


def add_freerooms(db, classroom_ids):
    for classroom_id in classroom_ids:
        for day in range(1, 6):
            for hour in range(7, 21):
                db.session.add(Freeroom(
                    day=day,
                    hour=hour,
                    classroom_id=classroom_id,
                ))

    db.session.commit()


def db_seed(db, user_manager):
    # Sample classrooms
    classrooms = [
        Classroom(name='Salón 4'),
        Classroom(name='Salón 5'),
        Classroom(name='Salón 7'),
        Classroom(name='Salón 8'),
        Classroom(name='Salón 6P'),
        Classroom(name='Salón 7P'),
        Classroom(name='Centro de cómputo'),
    ]

    db.session.add_all(classrooms)

    # Sample teachers
    db.session.add_all([
        Tutor(name='Ada Lovelace'),
        Tutor(name='Alan Turing'),
        Tutor(name='Margaret Hamilton'),
        Tutor(name='Alonzo Church'),
        Tutor(name='Grace Hopper'),
        Tutor(name='Noam Chomsky'),
    ])

    # Sample subjects
    db.session.add_all([
        Subject(nrc='1', tutor_id='1', hours='8', color='#137F1E', name='Introducción a la programación'),
        Subject(nrc='2', tutor_id='2', hours='8', color='#C83F3F', name='Teoría de la Complejidad'),
        Subject(nrc='3', tutor_id='3', hours='8', color='#C11EA6', name='Criptanálisis I'),
        Subject(nrc='4', tutor_id='4', hours='8', color='#394FB4', name='Sistemas embebidos'),
        Subject(nrc='5', tutor_id='5', hours='8', color='#88B439', name='Compiladores'),
        Subject(nrc='6', tutor_id='6', hours='8', color='#E3D744', name='Microcontroladores'),
        Subject(nrc='7', tutor_id='1', hours='8', color='#E34446', name='Programación funcional'),
        Subject(nrc='8', tutor_id='2', hours='8', color='#1B18BE', name='Arquitectura de Hardware'),
        Subject(nrc='9', tutor_id='3', hours='8', color='#49D5D3', name='Bases de datos I'),
    ])

    # Sample schedule
    db.session.add_all([
        Hour(hour=7, day=1, subject_id=1, classroom_id=1),
        Hour(hour=7, day=2, subject_id=1, classroom_id=1),
        Hour(hour=7, day=4, subject_id=1, classroom_id=2),
        Hour(hour=8, day=1, subject_id=1, classroom_id=1),
        Hour(hour=8, day=2, subject_id=1, classroom_id=1),
        Hour(hour=8, day=3, subject_id=3, classroom_id=3),
        Hour(hour=9, day=3, subject_id=3, classroom_id=3),
        Hour(hour=9, day=5, subject_id=3, classroom_id=4),
        Hour(hour=10, day=1, subject_id=3, classroom_id=4),
        Hour(hour=10, day=2, subject_id=3, classroom_id=4),
        Hour(hour=10, day=5, subject_id=3, classroom_id=4),
        Hour(hour=11, day=1, subject_id=3, classroom_id=4),
        Hour(hour=11, day=2, subject_id=3, classroom_id=4),
        Hour(hour=7, day=2, subject_id=4, classroom_id=2),
        Hour(hour=7, day=3, subject_id=4, classroom_id=1),
        Hour(hour=7, day=4, subject_id=4, classroom_id=1),
        Hour(hour=7, day=5, subject_id=4, classroom_id=1),
        Hour(hour=8, day=2, subject_id=4, classroom_id=2),
        Hour(hour=8, day=3, subject_id=4, classroom_id=1),
        Hour(hour=8, day=4, subject_id=4, classroom_id=1),
        Hour(hour=9, day=1, subject_id=4, classroom_id=1),
        Hour(hour=9, day=1, subject_id=5, classroom_id=5),
        Hour(hour=9, day=2, subject_id=5, classroom_id=5),
        Hour(hour=9, day=3, subject_id=5, classroom_id=5),
        Hour(hour=9, day=4, subject_id=5, classroom_id=5),
        Hour(hour=9, day=5, subject_id=5, classroom_id=5),
        Hour(hour=10, day=1, subject_id=5, classroom_id=5),
        Hour(hour=10, day=2, subject_id=5, classroom_id=5),
        Hour(hour=10, day=5, subject_id=5, classroom_id=5),
        Hour(hour=7, day=1, subject_id=6, classroom_id=6),
        Hour(hour=7, day=3, subject_id=6, classroom_id=6),
        Hour(hour=7, day=5, subject_id=6, classroom_id=6),
        Hour(hour=8, day=1, subject_id=6, classroom_id=6),
        Hour(hour=8, day=3, subject_id=6, classroom_id=6),
        Hour(hour=8, day=5, subject_id=6, classroom_id=6),
        Hour(hour=9, day=1, subject_id=6, classroom_id=6),
        Hour(hour=10, day=1, subject_id=6, classroom_id=6),
        Hour(hour=7, day=3, subject_id=7, classroom_id=7),
        Hour(hour=7, day=5, subject_id=7, classroom_id=7),
        Hour(hour=8, day=3, subject_id=7, classroom_id=7),
        Hour(hour=8, day=5, subject_id=7, classroom_id=7),
        Hour(hour=9, day=5, subject_id=7, classroom_id=7),
        Hour(hour=10, day=5, subject_id=7, classroom_id=7),
        Hour(hour=7, day=2, subject_id=8, classroom_id=6),
        Hour(hour=7, day=4, subject_id=8, classroom_id=6),
        Hour(hour=8, day=2, subject_id=8, classroom_id=6),
        Hour(hour=8, day=4, subject_id=8, classroom_id=6),
        Hour(hour=9, day=3, subject_id=8, classroom_id=6),
        Hour(hour=9, day=5, subject_id=8, classroom_id=6),
        Hour(hour=10, day=3, subject_id=8, classroom_id=6),
        Hour(hour=10, day=5, subject_id=8, classroom_id=6),
        Hour(hour=8, day=1, subject_id=9, classroom_id=4),
        Hour(hour=8, day=2, subject_id=9, classroom_id=4),
        Hour(hour=8, day=3, subject_id=9, classroom_id=4),
        Hour(hour=9, day=1, subject_id=9, classroom_id=4),
        Hour(hour=9, day=2, subject_id=9, classroom_id=4),
        Hour(hour=9, day=3, subject_id=9, classroom_id=4),
        Hour(hour=10, day=3, subject_id=9, classroom_id=4),
        Hour(hour=11, day=3, subject_id=9, classroom_id=4),
        Hour(hour=13, day=1, subject_id=2, classroom_id=6),
        Hour(hour=13, day=2, subject_id=2, classroom_id=1),
        Hour(hour=13, day=3, subject_id=2, classroom_id=1),
        Hour(hour=13, day=4, subject_id=2, classroom_id=1),
        Hour(hour=13, day=5, subject_id=2, classroom_id=1),
        Hour(hour=14, day=3, subject_id=2, classroom_id=1),
        Hour(hour=14, day=4, subject_id=2, classroom_id=1),
        Hour(hour=14, day=5, subject_id=2, classroom_id=1),
    ])

    # A user
    db.session.add(User(
        name='Admin',
        username='admin',
        password=user_manager.hash_password('123456'),
        email='categulario@mailinator.com',
        email_confirmed_at=datetime.now(),
        active=True,
    ))

    db.session.commit()

    compute_freehours(db)


def compute_freehours(db):

    db.session.query(Freeroom).delete()

    db.session.commit()

    add_freerooms(db, [c.id for c in Classroom.query.all()])

    for hour in Hour.query.all():
        for room in Freeroom.query.filter(Freeroom.hour == hour.hour).filter(Freeroom.day == hour.day).filter(Freeroom.classroom_id == hour.classroom_id):
            db.session.delete(room)

    db.session.commit()
