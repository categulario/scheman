import re
import sys

from sqlalchemy import Column, Integer, String, ForeignKey, DateTime, Boolean
from sqlalchemy.orm import relationship
from flask_user import UserMixin

from scheman import db


class ApiFilter:

    filters = []

    embeddable = []

    public = []

    def __json__(self, requested_embed):
        res = {}
        first_pieces = list(map(
            lambda x: x.split('.')[0],
            requested_embed
        ))

        for attr in self.public:
            if attr in self.embeddable and attr not in first_pieces:
                continue

            value = getattr(self, attr)

            if isinstance(value, ApiFilter):
                res[attr] = value.__json__(tuple(filter(
                    lambda x: x,
                    map(
                        lambda x: x.split('.')[1],
                        map(
                            lambda x: x + '.',
                            requested_embed
                        )
                    )
                )))
            else:
                res[attr] = getattr(self, attr)

        return res

    @staticmethod
    def get_class(string):
        return getattr(sys.modules[__name__], string)

    @classmethod
    def api_filter(cls, request):
        query = cls.query

        requested_embed = tuple(filter(
            lambda x: x,
            map(
                lambda x: x.strip(),
                request.args.get('embed', '').split(',')
            )
        ))

        for arg in requested_embed:
            if arg in cls.embeddable:
                query = query.join(getattr(cls, arg))

            match = re.match(r'^([a-z_]+)\.([a-z_]+)$', arg)
            if match and match.group(1) in cls.embeddable:
                child_class = cls.get_class(match.group(1).capitalize())

                if match.group(2) in child_class.embeddable:
                    query = query.join(getattr(cls, match.group(1)))
                    query = query.join(getattr(child_class, match.group(2)))

        if 'freeroom' in request.args:
            pass
        else:
            for arg in request.args:
                if arg in cls.filters:
                    query = query.filter(getattr(cls, arg) == request.args.get(arg))

                match = re.match(r'^([a-z_]+)\.([a-z_]+)$', arg)
                if match and match.group(1) in cls.embeddable:
                    child_class = cls.get_class(match.group(1).capitalize())

                    if match.group(2) in child_class.filters:
                        query = query.join(getattr(cls, match.group(1)))
                        query = query.filter(getattr(child_class, match.group(2)) == request.args.get(arg))

        return list(map(
            lambda x: x.__json__(requested_embed),
            query
        ))


class User(db.Model, UserMixin):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    username = Column(String(50), nullable=False, unique=True)
    password = Column(String(255), nullable=False, server_default='')
    email = Column(String(255), nullable=False, unique=True)
    email_confirmed_at = Column(DateTime())
    active = Column(Boolean(), nullable=False, server_default='0')
    name = Column(String(255), server_default='')


class Classroom(db.Model, ApiFilter):
    __tablename__ = 'classroom'

    id = Column(Integer, primary_key=True)
    name = Column(String(50), unique=True)

    schedule = relationship('Hour', back_populates='classroom', cascade='all, delete-orphan')
    freehours = relationship('Freeroom', back_populates='classroom', cascade='all, delete-orphan')

    public = [
        'id',
        'name',
    ]

    def __init__(self, name=None):
        self.name = name

    def __repr__(self):
        return '<Classroom %r>' % (self.name)


class Tutor(db.Model, ApiFilter):
    __tablename__ = 'tutor'

    id = Column(Integer, primary_key=True)
    name = Column(String(150), unique=True)

    subjects = relationship('Subject', back_populates='tutor')

    public = [
        'id',
        'name',
    ]

    def __init__(self, name=None):
        self.name = name

    def __repr__(self):
        return '<Tutor %r>' % self.name


class Subject(db.Model, ApiFilter):
    __tablename__ = 'subject'

    id = Column(Integer, primary_key=True)
    name = Column(String(150), unique=True)
    nrc = Column(String(20), unique=True)
    tutor_id = Column(Integer, ForeignKey('tutor.id'))
    hours = Column(Integer, default=0)
    color = Column(String(7), unique=True)

    tutor = relationship('Tutor', back_populates='subjects')
    schedule = relationship('Hour', back_populates='subject', cascade='all, delete-orphan')

    public = [
        'id',
        'name',
        'nrc',
        'tutor_id',
        'tutor',
        'hours',
        'color',
    ]

    filters = [
        'tutor_id',
    ]

    embeddable = [
        'tutor',
    ]

    def __init__(self, name=None, nrc=None, tutor_id=None, hours=None, color=None):
        self.name = name
        self.nrc = nrc
        self.tutor_id = tutor_id
        self.hours = hours
        self.color = color

    def __repr__(self):
        return '<Subject %r>' % self.name

    def autocomplete_name(self):
        return '{1} {0}'.format(self.name, self.nrc)


class Hour(db.Model, ApiFilter):
    __tablename__ = 'schedule'

    id = Column(Integer, primary_key=True)
    hour = Column(Integer)
    day = Column(Integer)
    subject_id = Column(Integer, ForeignKey('subject.id'))
    classroom_id = Column(Integer, ForeignKey('classroom.id'))

    subject = relationship('Subject', back_populates='schedule')
    classroom = relationship('Classroom', back_populates='schedule')

    public = [
        'id',
        'hour',
        'day',
        'subject_id',
        'classroom_id',

        # embeddable
        'subject',
        'classroom',
    ]

    filters = [
        'classroom_id',
        'subject_id',
        'hour',
        'day',
    ]

    embeddable = [
        'subject',
        'classroom',
    ]

    def __init__(self, hour=None, day=None, subject_id=None, classroom_id=None):
        self.hour = hour
        self.day = day
        self.subject_id = subject_id
        self.classroom_id = classroom_id

    def __repr__(self):
        return '<Hour %s %s a las %s el dia %s>' % (
            self.subject.name,
            self.classroom.name,
            self.hour,
            self.day,
        )


class Freeroom(db.Model, ApiFilter):
    __tablename__ = 'freeroom'

    id = Column(Integer, primary_key=True)
    hour = Column(Integer)
    day = Column(Integer)
    classroom_id = Column(Integer, ForeignKey('classroom.id'))

    classroom = relationship('Classroom', back_populates='freehours')

    public = [
        'id',
        'hour',
        'day',
        'classroom_id',

        # embeddable
        'classroom',
    ]

    embeddable = [
        'classroom',
    ]

    def __init__(self, hour=None, day=None, classroom_id=None):
        self.hour = hour
        self.day = day
        self.classroom_id = classroom_id

    def __repr__(self):
        return '<Freeroom %s at %s %s>' % (
            self.classroom.name,
            self.day,
            self.hour,
        )
