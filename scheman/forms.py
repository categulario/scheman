from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField
from wtforms.widgets import html_params, HTMLString
from wtforms.validators import DataRequired, ValidationError, Regexp
from scheman.database.models import Tutor, Classroom, Subject


class TextControl:
    def __init__(self, section):
        self.section = section

    def __call__(self, field, **kwargs):
        field_id = "{}-{}".format(self.section, kwargs.pop('id', field.id))
        field_label = kwargs.pop('label', field.label)
        field_name = kwargs.pop('name', field.name)
        field_placeholder = kwargs.pop('placeholder', '')
        field_type = kwargs.pop('type', 'text')
        field_readonly = kwargs.pop('readonly', False)
        field_autofocus = kwargs.pop('autofocus', False)

        params = {
            'id': field_id,
            'name': field_name,
            'placeholder': field_placeholder,
            'type': field_type,
            'value': field.data or '',
        }

        if field_readonly:
            params['readonly'] = 'readonly'

        return HTMLString("""<div class="form-group {error_class}">
            <label for="{id}">{label}</label>
            <input class="form-control" {params} {autofocus}>
            <ul class="form-errors">
                {errors}
            </ul>
        </div>""".format(
            id=field_id,
            label=field_label,
            error_class='has-error' if field.errors else '',
            errors=''.join(map(lambda x: '<li>{}</li>'.format(x), field.errors)),
            params=html_params(**params),
            autofocus='autofocus' if field_autofocus else '',
        ))


class ForeignControl:
    def __init__(self, section, source):
        self.section = section
        self.source = source

    def __call__(self, field, **kwargs):
        field_id = "{}-{}".format(self.section, kwargs.pop('id', field.id))
        field_label = kwargs.pop('label', field.label)
        field_name = kwargs.pop('name', field.name)
        field_placeholder = kwargs.pop('placeholder', '')
        field_type = kwargs.pop('type', 'text')
        field_readonly = kwargs.pop('readonly', False)

        params = {
            'id': field_id,
            'name': field_name,
            'placeholder': field_placeholder,
            'type': field_type,
        }

        if field_readonly:
            params['readonly'] = 'readonly'

        return HTMLString("""<div class="form-group {error_class}">
            <label for="{id}">{label}</label>
            <select class="form-control" {params}>
                {options}
            </select>
            <ul class="form-errors">
                {errors}
            </ul>
        </div>""".format(
            id=field_id,
            label=field_label,
            error_class='has-error' if field.errors else '',
            errors=''.join(map(lambda x: '<li>{}</li>'.format(x), field.errors)),
            params=html_params(**params),
            options=''.join(map(
                lambda x: '<option value="{id}" {selected}>{label}</option>'.format(
                    id=x.id,
                    label=x.name,
                    selected='selected = "selected"' if x.id == field.data else '',
                ),
                self.source.query.all(),
            )),
        ))


class ClassroomForm(FlaskForm):
    name = StringField('name', validators=[DataRequired()], widget=TextControl('classroom'))

    def validate_name(self, field):
        if Classroom.query.filter(Classroom.name == field.data).first():
            raise ValidationError('Ya existe un registro con este valor')

    def clear(self):
        self.name.data = ''


class TutorForm(FlaskForm):
    name = StringField('name', validators=[DataRequired()], widget=TextControl('tutor'))

    def validate_name(self, field):
        if Tutor.query.filter(Tutor.name == field.data).first():
            raise ValidationError('Ya existe un registro con este valor')

    def clear(self):
        self.name.data = ''


class SubjectForm(FlaskForm):
    name = StringField('name', validators=[DataRequired(message='Campo requerido')], widget=TextControl('subject'))
    tutor_id = IntegerField('tutor_id', validators=[DataRequired(message='Campo requerido')], widget=ForeignControl('subject', Tutor))
    nrc = StringField('nrc', validators=[
        DataRequired(message='Campo requerido'),
    ], widget=TextControl('subject'))
    color = StringField('color', validators=[
        DataRequired(message='Campo requerido'),
        Regexp(regex=r'^#[0-9a-f]{6}$', message='El formato de un color es como #345678'),
    ], widget=TextControl('subject'))
    hours = IntegerField('hours', validators=[DataRequired(message='Campo requerido')], widget=TextControl('subject'))

    edit = False

    def __init__(self, *args, **kwargs):
        if 'edit' in kwargs:
            self.edit = kwargs['edit']
        super().__init__(*args, **kwargs)

    def validate_name(self, field):
        if not self.edit and Subject.query.filter(Subject.name == field.data).first():
            raise ValidationError('Ya existe un registro con este nombre')

    def validate_color(self, field):
        if not self.edit and Subject.query.filter(Subject.color == field.data).first():
            raise ValidationError('Ya existe un registro con este color')

    def validate_nrc(self, field):
        if not self.edit and Subject.query.filter(Subject.nrc == field.data).first():
            raise ValidationError('Ya existe un registro con este NRC')

    def clear(self):
        self.name.data = ''
        self.tutor_id.data = ''
        self.nrc.data = ''
        self.color.data = ''
        self.hours.data = ''
