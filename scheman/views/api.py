from flask import Blueprint, request, jsonify
from scheman.database.models import Classroom, Tutor, Subject, Hour, Freeroom

bp = Blueprint('api', __name__, url_prefix='/api')


def for_autocomplete(item):
    return {
        'id': '{}/{}'.format(type(item).__name__.lower(), item.id),
        'text': item.autocomplete_name() if hasattr(item, 'autocomplete_name') else item.name,
    }


@bp.route('/all')
def api_all():
    """returns every object registered"""
    free_rooms = [{
        'id': 'freerooms',
        'text': 'Salones libres',
    }]
    tutors = list(map(for_autocomplete, Tutor.query.all()))
    classrooms = list(map(for_autocomplete, Classroom.query.all()))
    subjects = list(map(for_autocomplete, Subject.query.all()))

    return jsonify(
        free_rooms + tutors + classrooms + subjects
    )


@bp.route('/schedule')
def api_schedule():
    return jsonify(Hour.api_filter(request))


@bp.route('/freeroom')
def api_freeroom():
    return jsonify(Freeroom.api_filter(request))
