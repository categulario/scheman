import re

from flask import Blueprint, render_template, request, redirect, url_for
from flask_user import login_required
from sqlalchemy.orm import joinedload

from scheman import db
from scheman.database.models import Classroom, Tutor, Subject, Hour, Freeroom
from scheman.forms import ClassroomForm, TutorForm, SubjectForm

bp = Blueprint('web', __name__)


@bp.route('/')
def index():
    return redirect(url_for('web.schedule'))


@bp.route('/schedule')
def schedule(object=None, id=None):
    return render_template('schedule.html')


@bp.route('/myschedule')
def myschedule():
    return render_template('myschedule.html')


@bp.route('/subjects', methods=['GET', 'POST'])
@login_required
def subjects(id=None):
    form = SubjectForm()

    if form.validate_on_submit():
        subject = Subject(
            name=request.form['name'],
            nrc=request.form['nrc'],
            tutor_id=request.form['tutor_id'],
            hours=request.form['hours'],
            color=request.form['color'],
        )
        db.session.add(subject)
        db.session.commit()

        for hdata in request.form.getlist('hour'):
            g = re.match(r'h(?P<hour>[0-9]+)-d(?P<day>[0-9]+)', hdata)
            hour = Hour(
                hour=g.group('hour'),
                day=g.group('day'),
                subject_id=subject.id,
                classroom_id=request.form['day_room_{}'.format(g.group('day'))],
            )
            db.session.add(hour)

            # Mark this classrooms as not available
            Freeroom \
                .query \
                .filter(Freeroom.hour == hour.hour) \
                .filter(Freeroom.day == hour.day) \
                .filter(Freeroom.classroom_id == hour.classroom_id) \
                .delete()

        db.session.commit()
        form.clear()

    return render_template(
        'subjects.html',
        subjects=Subject.query.options(joinedload(Subject.tutor)).all(),
        classrooms=Classroom.query.all(),
        tutors=Tutor.query.all(),
        form=form,
    )


@bp.route('/subjects/<id>', methods=['GET', 'POST'])
@login_required
def subject_edit(id):
    subject = Subject.query.get(id)
    form = SubjectForm(obj=subject, edit=True)

    if form.validate_on_submit():
        form.populate_obj(subject)

        for hour in subject.schedule:
            # check for other hours. If none, add an entry for this classroom
            other_hours = Hour \
                .query \
                .filter(Hour.hour == hour.hour) \
                .filter(Hour.day == hour.day) \
                .filter(Hour.classroom_id == hour.classroom_id) \
                .filter(Hour.id != hour.id) \
                .first()

            if other_hours is None:
                # No subject in this hour/day/classroom, add a freeroom
                db.session.add(Freeroom(
                    hour=hour.hour,
                    day=hour.day,
                    classroom_id=hour.classroom_id,
                ))

        Hour.query.filter_by(subject_id=id).delete()

        for hdata in request.form.getlist('hour'):
            g = re.match(r'h(?P<hour>[0-9]+)-d(?P<day>[0-9]+)', hdata)
            hour = Hour(
                hour=g.group('hour'),
                day=g.group('day'),
                subject_id=subject.id,
                classroom_id=request.form['day_room_{}'.format(g.group('day'))],
            )
            db.session.add(hour)

            # Mark this classrooms as not free
            Freeroom \
                .query \
                .filter(Freeroom.hour == hour.hour) \
                .filter(Freeroom.day == hour.day) \
                .filter(Freeroom.classroom_id == hour.classroom_id) \
                .delete()

        db.session.commit()

        return redirect(url_for('web.subjects'))

    return render_template(
        'subjects.html',
        subjects=Subject.query.all(),
        classrooms=Classroom.query.all(),
        tutors=Tutor.query.all(),
        form=form,
        edit=True,
    )


@bp.route('/subjects/<id>/delete', methods=['POST'])
@login_required
def subject_delete(id):
    subject = Subject.query.get(id)

    for hour in subject.schedule:
        # check for other hours. If none, add an entry for this classroom
        other_hours = Hour \
            .query \
            .filter(Hour.hour == hour.hour) \
            .filter(Hour.day == hour.day) \
            .filter(Hour.classroom_id == hour.classroom_id) \
            .filter(Hour.id != hour.id) \
            .first()

        if other_hours is None:
            # No subject in this hour/day/classroom, add a freeroom
            db.session.add(Freeroom(
                hour=hour.hour,
                day=hour.day,
                classroom_id=hour.classroom_id,
            ))

    db.session.delete(subject)
    db.session.commit()

    return redirect(url_for('web.subjects'))


@bp.route('/tutors', methods=['GET', 'POST'])
@login_required
def tutors(id=None):
    form = TutorForm()

    if form.validate_on_submit():
        tutor = Tutor(request.form['name'])
        db.session.add(tutor)
        db.session.commit()
        form.clear()

    return render_template(
        'tutors.html',
        tutors=Tutor.query.all(),
        form=form,
    )


@bp.route('/tutors/<id>', methods=['GET', 'POST'])
@login_required
def tutor_edit(id):
    tutor = Tutor.query.get(id)
    form = TutorForm(obj=tutor)

    if form.validate_on_submit():
        form.populate_obj(tutor)
        db.session.commit()

        return redirect(url_for('web.tutors'))

    return render_template(
        'tutors.html',
        tutors=Tutor.query.all(),
        form=form,
        edit=True,
    )


@bp.route('/tutors/<id>/delete', methods=['POST'])
@login_required
def tutor_delete(id):
    tutor = Tutor.query.get(id)

    db.session.delete(tutor)
    db.session.commit()

    return redirect(url_for('web.tutors'))


@bp.route('/classrooms', methods=['GET', 'POST'])
@login_required
def classrooms():
    form = ClassroomForm()

    if form.validate_on_submit():
        classroom = Classroom(request.form['name'])
        db.session.add(classroom)
        db.session.commit()
        form.clear()

    return render_template(
        'classrooms.html',
        classrooms=Classroom.query.all(),
        form=form,
    )


@bp.route('/classrooms/<id>', methods=['GET', 'POST'])
@login_required
def classroom_edit(id):
    classroom = Classroom.query.get(id)
    form = ClassroomForm(obj=classroom)

    if form.validate_on_submit():
        form.populate_obj(classroom)
        db.session.commit()

        return redirect(url_for('web.classrooms'))

    return render_template(
        'classrooms.html',
        classrooms=classroom.query.all(),
        form=form,
        edit=True,
    )


@bp.route('/classrooms/<id>/delete', methods=['POST'])
@login_required
def classroom_delete(id):
    classroom = Classroom.query.get(id)

    db.session.delete(classroom)
    db.session.commit()

    return redirect(url_for('web.classrooms'))


@bp.route('/help')
def help():
    return render_template('help.html')


@bp.route('/export')
@login_required
def export():
    return render_template('export.html')
