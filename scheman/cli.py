from datetime import datetime
from getpass import getpass

import click
from flask.cli import with_appcontext
from flask import current_app

from . import db
from .database.ops import db_seed, compute_freehours
from .database.models import User


@click.command('migrate')
@with_appcontext
def migrate():
    """Initialize the database."""
    db.create_all()
    click.echo('Database migrated')


@click.command('seed')
@with_appcontext
def seed():
    """Seed the database"""
    db_seed(db, current_app.user_manager)
    click.echo('Database seeded')


@click.command('free')
@with_appcontext
def free():
    """Compute the free hours"""
    click.echo('Computing... please wait')
    compute_freehours(db)
    click.echo('Done!')


@click.command()
@with_appcontext
def create_user():
    user = User(
        name=input('Name: '),
        username=input('Username: '),
        password=current_app.user_manager.hash_password(getpass('Password: ')),
        email=input('Email: '),
        email_confirmed_at=datetime.now(),
        active=True,
    )
    db.session.add(user)
    db.session.commit()
