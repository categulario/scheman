import os

from flask import Flask
from flask_mail import Mail
from flask_sqlalchemy import SQLAlchemy
from flask_user import UserManager

mail = Mail()
db = SQLAlchemy()


def create_app(test_config=None):
    app = Flask(__name__)

    if test_config is None:
        app.config.from_object('scheman.settings')

        if os.getenv('SCHEMAN_SETTINGS') is not None:
            app.config.from_envvar('SCHEMAN_SETTINGS', silent=False)
    else:
        app.config.from_mapping(test_config)

    # Initialize external dependencies
    mail.init_app(app)
    db.init_app(app)

    from .database.models import User
    UserManager(app, db, User)

    # Load views
    from scheman.views.web import bp
    app.register_blueprint(bp)

    from scheman.views.api import bp
    app.register_blueprint(bp)

    # Load cli commands
    from .cli import migrate, seed, free, create_user

    app.cli.add_command(migrate)
    app.cli.add_command(seed)
    app.cli.add_command(free)
    app.cli.add_command(create_user)

    return app
