#!/bin/bash

__dir=$(dirname $(realpath $0))
container=scheman
image=registry.gitlab.com/categulario/scheman/app:latest

if [[ -z $@ ]]; then
    cmd=$image
    name="--name=$container"

    podman rm -i $container
else
    cmd="$image $@"
fi

podman run \
    -it --rm \
    --env-file $__dir/../../.env \
    --publish 8000:8000 \
    $name \
    $cmd
