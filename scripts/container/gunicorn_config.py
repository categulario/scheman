import os

bind = ['0.0.0.0:8000']
worker_class = 'eventlet'
workers = int(os.getenv('APP_WORKERS', '3'))
