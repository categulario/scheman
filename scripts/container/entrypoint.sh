#!/bin/bash
# For use inside the container

set -e

if [[ -z "$@" ]]; then
    flask migrate

    exec gunicorn -c $APP_CONFIG/gunicorn_config.py "scheman:create_app()"
else
    exec $@
fi
