import pytest


def test_home(client):
    response = client.get('/')

    assert response.status_code == 302
    assert response.headers['Location'] == 'http://localhost/schedule'


def test_schedule(client):
    response = client.get('/schedule')

    assert response.status_code == 200


@pytest.mark.skip(reason='needs auth testing structure')
def test_subjects(client, auth):
    response = client.get('/subjects')

    assert response.status_code == 200


@pytest.mark.skip(reason='needs auth testing structure')
def test_subject_view(client):
    response = client.get('/subjects/1')

    assert response.status_code == 200


@pytest.mark.skip(reason='needs auth testing structure')
def test_subject_update(client):
    response = client.post('/subjects/1')

    assert response.status_code == 200


@pytest.mark.skip(reason='needs auth testing structure')
def test_subject_delete(client):
    response = client.post('/subjects/1')

    assert response.status_code == 200


@pytest.mark.skip(reason='needs auth testing structure')
def test_tutors(client):
    response = client.get('/tutors')

    assert response.status_code == 200


@pytest.mark.skip(reason='needs auth testing structure')
def test_tutor_view(client):
    response = client.get('/tutors/1')

    assert response.status_code == 200


@pytest.mark.skip(reason='needs auth testing structure')
def test_tutor_update(client):
    response = client.post('/tutors/1')

    assert response.status_code == 200


@pytest.mark.skip(reason='needs auth testing structure')
def test_tutor_delete(client):
    response = client.delete('/tutors/1')

    assert response.status_code == 200


@pytest.mark.skip(reason='needs auth testing structure')
def test_classrooms(client):
    response = client.delete('/tutors/1')

    assert response.status_code == 200


@pytest.mark.skip(reason='needs auth testing structure')
def test_classroom_view(client):
    response = client.get('/classroom/1')

    assert response.status_code == 200


@pytest.mark.skip(reason='needs auth testing structure')
def test_classroom_delete(client):
    response = client.delete('/classroom/1')

    assert response.status_code == 200


@pytest.mark.skip(reason='needs auth testing structure')
def test_help(client):
    response = client.get('/help')

    assert response.status_code == 200


@pytest.mark.skip(reason='needs auth testing structure')
def test_export(client):
    response = client.get('/export')

    assert response.status_code == 200
