import os
import tempfile

import pytest

from scheman import create_app
from scheman import db


@pytest.fixture
def client():
    scheman = create_app({
        'TESTING': True,
        'SECRET_KEY': 'asJHVl3VTv8mpvgFQPpgnsXvGmhhNbnQ',
        'SQLALCHEMY_TRACK_MODIFICATIONS': False,
        'SQLALCHEMY_DATABASE_URI': 'sqlite:///testing.sqlite',
        'MAIL_DEFAULT_SENDER': '"MyApp" <noreply@example.com>',
    })

    with scheman.test_client() as client:
        with scheman.app_context():
            db.create_all()
        yield client
